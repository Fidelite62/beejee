<?php

function __autoload($class_name)
{
    $arrya_paths = array(
        '/models/',
        '/components/'
    );

    foreach ($arrya_paths as $path) {
        $path = ROOT . $path . $class_name . '.php';
        if (is_file($path)) {
            include_once $path;
        }
    }
}
