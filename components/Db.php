<?php

Class Db
{
    public static function getConnection()
    {
        //TODO add new file with DB settings in config
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "beejee";

        $conn = mysqli_connect($servername, $username, $password, $dbname);

        mysqli_set_charset($conn,"utf8");

        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        return $conn;
    }


}