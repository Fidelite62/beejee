<?php

return array(

    'exercises/create' => 'exercise/create',
    'exercises/edit' => 'exercise/edit',
//    'exercises/([0-9]+)' => 'exercise/view',
    'exercises' => 'exercise/index',  // route => controller/action

    'admin/login' => 'user/login',

);