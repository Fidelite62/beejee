<?php

class ExerciseController
{
    public function actionIndex()
    {
        $exerciseList = array();
        $exerciseList = Exercise::getExerciseList();

        require_once(ROOT . '/views/exercise/index.php');


        return true;
    }

    public function actionView()
    {
//         "TODO single exercise";
        return true;
    }

    public function actionCreate()
    {
        $name = '';
        $email = '';
        $title = '';
        $content = '';
        $image = '';

        $result = false;

        if (isset($_POST['name'])) {
            $name = $_POST['name'];
            $email = $_POST['email'];
            $title = $_POST['title'];
            $content = $_POST['content'];
            $image = $_POST['image'];


            $errors = false;

            if (!Exercise::checkName($name)) {
                $errors[] = 'Name must be longer than 2 char.';
            }

            if (!Exercise::checkEmail($email)) {
                $errors[] = 'email is not correct.';
            }

            if (!Exercise::checkTitle($title)) {
                $errors[] = 'title must be longer than 3 char.';
            }

            if (!Exercise::checkContent($content)) {
                $errors[] = 'Content must be longer than 10 char.';
            }

            if ($errors == false) {
                $result = Exercise::create($name, $email, $title, $content, $image);
                $result = true;
            }
//TODO UPDATE CRUD
        }

        require_once(ROOT . '/views/exercise/createExercise.php');

        return true;
    }

    public function actionEdit($id)
    {
//TODO
//        require_once(ROOT . '/views/exercise/editExercise.php');
    }

    public function actionDelete($id)
    {
        //TODO
//        echo $id;
//        Exercise::delete($id);
//        header('location'.'/exercises');
    }

}