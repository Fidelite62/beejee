<?php

class UserController
{
    public function actionLogin()
    {
        $email = '';
        $password = '';
        if (isset($_POST['submit'])) {
            $email = $_POST['email'];
            $password = $_POST['password'];
        }

        $errors = false;

        if (!User::checkEmail($email)) {
            $errors[] = 'Email is not correct.';
        }

        if (!User::checkPassword($password)) {
            $errors[] = 'Password must be longer than 2 char.';
        }

        $userId = User::checkUserData($email, $password);
//        echo $email."<br>";
//        echo $password;die;
//        var_dump($userId);die;
//        $userId = true;
        if ($userId == false) {
            $errors[] = 'incorrect data to enter the site';

        } else {
            User::auth($userId);
            header("Location:/exercises/");

        }

        require_once(ROOT . '/views/user/login.php');

        return true;
    }
}