<?php

class Exercise
{
    public static function getExerciseList()
    {

        $db = Db::getConnection();
        $exerciseList = array();
        $qwery = "SELECT * FROM exercise ORDER BY id DESC ";

        $result = mysqli_query($db, $qwery);
        $row = mysqli_fetch_assoc($result);

        $i = 0;
        while ($row = $result->fetch_assoc()) {
            $exerciseList[$i]['id'] = $row['id'];
            $exerciseList[$i]['name'] = $row['name'];
            $exerciseList[$i]['email'] = $row['email'];
            $exerciseList[$i]['title'] = $row['title'];
            $exerciseList[$i]['content'] = $row['content'];
            $exerciseList[$i]['image'] = $row['image'];
            $exerciseList[$i]['date'] = $row['date'];
            $exerciseList[$i]['status'] = $row['status'];
            $i++;
        }
        $db->close();
        return $exerciseList;
    }

    public static function getExerciseInfo()
    {
        $db = Db::getConnection();
        $qwery= "SELECT * FROM exercise";
        $result = mysqli_query($db,$qwery);

        $row=mysqli_fetch_assoc($result);

        return $row;
    }

    public static function getExerciseById($id)
    {
        //TODO
    }

    public static function create($name, $email, $title, $content, $image)
    {
        $conn = Db::getConnection();
        if (isset($_POST["name"])) {

            $stmt = $conn->prepare("INSERT INTO exercise (name, email, title, content,image) VALUES (?,?,?,?,?)");
            $stmt->bind_param("sssss", $name, $email, $title, $content, $image);

            $stmt->execute();

            $stmt->close();
            $conn->close();
        }
    }

    public static function update($id)
    {
        //TODO
        return true;
    }

    public static function delete($id)
    {
        //TODO
//        $conn = Db::getConnection()->prepare('DELETE FROM exercise WHERE id = :id');
//        $conn->execute(array(
//            ':id'=>$id
//        ));
    }

    public static function checkName($name)
    {
        if (strlen($name) >= 2) {
            return true;
        }
        return false;
    }

    public static function checkEmail($email)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return true;
        }
        return false;
    }

    public static function checkTitle($title)
    {
        if (strlen($title) >= 3) {
            return true;
        }
        return false;
    }

    public static function checkContent($content)
    {
        if (strlen($content) >= 10) {
            return true;
        }
        return false;
    }
}

