<?php

class User
{
    public static function auth($userId)
    {
        session_start();
        $_SESSION['user'] = $userId;
    }

    public static function checkUserData($email, $password)
    {
        $db = Db::getConnection();

        if (isset($_POST['email'])) {
            $mail = trim(strip_tags($_POST['email']));
        } else $mail = "";
        if (isset($_POST['password'])) {
            $pass = trim(strip_tags($_POST['password']));
        } else $pass = "";

        $qwery = "SELECT * FROM users WHERE email=('$mail') AND password=('$password')";
        $result = mysqli_query($db, $qwery);
//        echo  $db->error;

        while ($row = mysqli_fetch_assoc($result)) {
            if ($row) {
                header('Location:/exercises');
            }
//            echo $row['id']."<br>";
//            echo $row['email']."<br>";
//            echo $row['password']."<br>";
//            echo $row['role']."<br>";
        }
    }

    public static function checkEmail($email)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return true;
        }
        return false;
    }

    public static function checkPassword($password)
    {
        if (strlen($password) >= 6) {
            return true;
        }
        return false;
    }

    public static function getUserInfo()
    {
        $db = Db::getConnection();
        $qwery = "SELECT * FROM users";
        $result = mysqli_query($db, $qwery);

        $row = mysqli_fetch_assoc($result);

        return $row;
    }

    public static function isAdmin()
    {
        return 'admin' == User::getUserInfo()['role'];
    }

    public static function isUser()
    {
        return 'user' == User::getUserInfo()['role'];
    }
}
