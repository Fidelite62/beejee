<?php include ROOT . '/views/layouts/header.php'; ?>

<div class="main">
    <!-- Content Section Begins -->
    <div id="main-content" class="content-box blog-list no-sidebar pad-top-big pr">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="sub-title">
                            <br>
                            <h2>Add new Exercise:</h2>
                        </div>
                        <?php if ($result): ?>
                            <p>Exercise added! </p>
                            <a href='create' class="btn btn-danger  pull-right" role="button">Add one more exercise</a>
                            <br><br>
                        <?php else: ?>
                            <?php require_once ROOT . '/components/check.php' ?>
                            <div class="col-md-12 contact-form pad-top-big pad-bottom-big">
                                <div class="row">
                                    <form action="" method="post" enctype="multipart/form-data">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input type="text" name="name" class="form-control"
                                                       placeholder="Your Name"
                                                       value="<?php echo $name; ?>">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input type="email" name="email" class="form-control"
                                                       placeholder="Your Email" value="<?php echo $email; ?>">
                                            </div>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
                                                <input type="text" name="title" class="form-control" placeholder="Title"
                                                       value="<?php echo $title; ?>">
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name="image" class="form-control"
                                                       placeholder="Your img URL"
                                                       value="">
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="textarea-message form-group">
                                            <textarea name="content" class="textarea-message form-control"
                                                      placeholder="Your Exercise" rows="6"
                                                      value="<?php echo $content; ?>"></textarea>
                                            </div>
                                        </div>
                                        <div class="text-center">
                                            <button type="submit" class="btn btn-danger btn-lg">Send Exercise</button>
                                        </div>
                                        <br>

                                    </form>
                                </div>

                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Content Section Ends -->

<?php include ROOT . '/views/layouts/footer.php'; ?>
