<?php include ROOT . '/views/layouts/header.php'; ?>

<div class="main">
    <!-- Content Section Begins -->
    <div id="main-content" class="content-box blog-list no-sidebar pad-top-big pr">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="sub-title">
                            <br>
                            <h2>Edit Exercise:</h2>
                        </div>
                        <?php require_once ROOT . '/components/check.php' ?>
                        <div class="col-md-12 contact-form pad-top-big pad-bottom-big">
                            <div class="row">
                                <form action="" method="post" enctype="multipart/form-data">
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            Title: <input type="text" name="title" class="form-control"
                                                          placeholder="Title"
                                                          value="<?php echo Exercise::getExerciseInfo()['title'] ?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            Image: <input type="text" name="image" class="form-control"
                                                          placeholder="Your img URL"
                                                          value="<?php echo Exercise::getExerciseInfo()['image'] ?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="textarea-message form-group">
                                            Task: <textarea name="content" class="textarea-message form-control"
                                                            placeholder="Your Exercise" rows="6"
                                                            value=""><?php echo Exercise::getExerciseInfo()['content'] ?></textarea>
                                        </div>
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-danger btn-lg">Edit Exercise</button>
                                    </div>
                                    <br>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Content Section Ends -->

<?php include ROOT . '/views/layouts/footer.php';
exit();
?>
