<?php include ROOT . '/views/layouts/header.php'; ?>

    <!-- Parallax Section Begins -->
    <section id="banner" class="banner banner-static parallax-fix parallaximg">
        <div class="container pr">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="banner-text">
                        <div class="banner-cell">
                            <h1> Exercises <span>BeeJee</span></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Parallax Section Ends -->
    </div>
    <div class="main">
    <!-- Content Section Begins -->
    <div id="main-content" class="content-box blog-list no-sidebar pad-top-big pr">
        <div class="container">
            <div class="row">
                <div class="dropdown">
                    <button class="btn btn-primary dropdown-toggle " type="button" data-toggle="dropdown">Without
                        sorting
                        <span class="caret"></span></button>
                    <ul class="dropdown-menu sort">
                        <li><a href="" id="nameAsc">by name A-Z</a></li>
                        <li><a href="" id="nameDesc">by name Z-A</a></li>
                        <li><a href="" id="emailAsc">by email A-Z</a></li>
                        <li><a href="" id="emailDesc">by email Z-A</a></li>
                        <li><a href="" id="statusAsc">by status</a></li>
                    </ul>
                </div>
                <a href='/exercises/create' class="btn btn-danger pull-right" role="button">Add new exercise</a>
                <hr>
                <br><br><br>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <!-- Blog Pagination Begins -->
                    <?php
                    $count = 3;
                    $pageCounter = floor(count($exerciseList) / $count);
                    $p = isset($_GET["page"]) ? (int)$_GET["page"] : 0;

                    for ($i = $p * $count; $i < ($p + 1) * $count; $i++):
                        if (isset($exerciseList[$i])): ?>

                            <article class="outer-blog-box pr pad-bottom-small">
                                <div class="blog-img blog-media"><img src="<?php echo $exerciseList[$i]['image'] ?>"
                                                                      alt="images"/>
                                </div>
                                <div class="blog-captions">
                                    <h2 class="blog-title"><a href="#"><?php echo $exerciseList[$i]['title'] ?></a>
                                    </h2>
                                    <ul class="below-blog-list">
                                        <li>
                                            <a href="javascript:void(0)"> <i class="fa fa-user-o"
                                                                             aria-hidden="true"></i><?php echo $exerciseList[$i]['name'] ?>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)"> <i class="fa fa-calendar-o"
                                                                             aria-hidden="true"></i> <?php echo $exerciseList[$i]['date'] ?>
                                            </a>
                                        </li>
                                    </ul>
                                    <p> <?php echo $exerciseList[$i]['content'] ?></p>

                                    <div class="checkbox pull-right">
                                        <?php if ($exerciseList[$i]['status'] == 'in progress'): ?>
                                            <h4>
                                                <span class="label label-success"><?php echo $exerciseList[$i]['status'] ?></span>
                                            </h4>
                                        <?php else: ?>
                                            <h4>
                                                <span class="label label-danger"><?php echo $exerciseList[$i]['status'] ?></span>
                                            </h4>
                                        <?php endif; ?>
                                    </div>
                                    <?php if (User::isAdmin()) { ?>
                                        <a href='/exercises/edit' class="btn btn-danger " role="button"> <span
                                                    class="glyphicon glyphicon-edit" aria-hidden="true"></span> Edit
                                            exercise</a>

                                        <a href="/exercises/" class="btn btn-danger " role="button"> <span
                                                    class="glyphicon glyphicon-remove" aria-hidden="true"></span> Delete
                                        </a>
                                    <?php } ?>
                                </div>
                            </article>
                        <?php endif; ?>
                    <?php endfor; ?>

                    <nav>
                        <ul class="pagination">
                            <?php for ($i = 0; $i <= $pageCounter; $i++): ?>
                                <li><a href="?page=<?= $i ?>"><?= $i + 1 ?></a></li>
                            <?php endfor; ?>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- Content Section Ends -->

<?php include ROOT . '/views/layouts/footer.php'; ?>