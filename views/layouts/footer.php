<!-- footer Section Begins -->
<footer id="footer" class="footer pr pad-bottom-small text-align-center">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <!-- Subscribe Form Begins -->
                <div class="subscibe-box">
                    <h4>Get our latest exersices on email</h4>
                    <form class="subscribe-form">
                        <input type="text" placeholder="Enter your email here">
                        <a href="javascript:void(0)" class="subscribe-btn"><i class="fa fa-location-arrow"
                                                                              aria-hidden="true"></i></a>
                    </form>
                </div>
                <!-- Subscribe Form Ends -->
            </div>
        </div>
    </div>
</footer>
<!-- footer Section Ends -->
<!-- Copyright Section Begins -->
<div id="copyright" class="copyright pr text-align-center">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h5>&copy; 2017</h5>
            </div>
        </div>
    </div>
</div>
<!-- Copyright Section Ends -->
<!-- Back to top Section Begins -->
<a href="javascript:void(0);" class="back-to-top"><i class="fa fa-long-arrow-up" aria-hidden="true"></i></a>
<!-- Back to top Section Ends -->
</div>
</div>
<!-- Mobile Menu Section Begins -->
<div class="main_menucontent overlapblackbg"></div>
<div class="menu-wrap"></div>
<!-- Mobile Menu Section Ends -->
<!-- Jquery Js -->
<script type="text/javascript" src="../template/js/jquery.min.js"></script>
<!-- Bootstrap Js -->
<script type="text/javascript" src="../template/js/bootstrap.min.js"></script>
<!-- Modernizr Js -->
<script type="text/javascript" src="../template/js/modernizr.js"></script>
<!-- Owl Carousel Js -->
<script type="text/javascript" src="../template/js/owl.carousel.js"></script>
<!-- Localscroll Js -->
<script type="text/javascript" src="../template/js/jquery.localscroll-1.2.7-min.js"></script>
<!--Imagesloaded Js-->
<script type="text/javascript" src="../template/js/imagesloaded.pkgd.min.js"></script>
<!-- Isotope Js -->
<script type="text/javascript" src="../template/js/jquery.isotope.min.js"></script>
<!-- Parallax Js -->
<script type="text/javascript" src="../template/js/jquery.parallax-1.1.3.js"></script>
<!-- Magnific Popup Js -->
<script type="text/javascript" src="../template/js/jquery.magnific-popup.js"></script>
<!-- Custom Js -->
<script type="text/javascript" src="../template/js/element.js"></script>
</body>
</html>
