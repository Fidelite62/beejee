<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0">

    <title> Exercises </title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
    <link type="text/css" rel="stylesheet" href="../template/css/bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="../template/css/owl.carousel.min.css">
    <link type="text/css" rel="stylesheet" href="../template/css/font-awesome.css">
    <link type="text/css" href="../template/css/magnific-popup.css" rel="stylesheet"/>
    <!-- Custom css -->
    <link type="text/css" href="../template/css/style.css?v=1.0" rel="stylesheet"/>
    <!-- Custom Media Query css -->
    <link type="text/css" href="../template/css/media.css?v=1.0" rel="stylesheet"/>

</head>

<body id="mainBox" data-spy="scroll" data-target="#main-menu" data-offset="150">
<div class="blog-main">
    <div id="site-head">
        <header id="header" class="header-block" data-top="0" data-scroll="100">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 main-menu">
                        <div class="menuBar scrollbtn">
                            <!-- Navigation Menu Begins -->
                            <nav class="navbar navbar-default">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                            data-target="#main-menu" aria-expanded="false">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                    <!-- Site Logo Begins -->
                                    <div class="logo">
                                        <!--                                        TODO  add new address-->
                                        <a class="navbar-brand" href="/exercises"><img src="../template/images/logo.png"
                                                                                       alt="logo"/></a>
                                    </div>
                                    <!-- Site Logo Ends -->
                                </div>
                                <div class="home-menu">
                                    <div class="collapse navbar-collapse" id="main-menu">
                                        <ul class="nav navbar-nav navbar-right">
                                            <li><a href="/exercises">Go to Exercise</a></li>
                                            <li><a href="#"><h6>hello, <?php echo User::getUserInfo()['email'] ?></h6>
                                                </a></li>
                                        </ul>
                                    </div>
                                </div>
                            </nav>
                            <!-- Navigation Menu Ends -->
                        </div>
                    </div>
                </div>
            </div>
        </header>