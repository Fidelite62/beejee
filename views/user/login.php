<?php include ROOT . '/views/layouts/header.php'; ?>

<div class="main">
    <br>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <!-- Contact Form Section Begins -->
                <div class="contact-form pad-top-big pad-bottom-big">
                    <h2>LogIn, please</h2>
                    <?php require_once ROOT . '/components/check.php' ?>
                    <form action='#' method="post">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group">
                                <input type="email" name="email" placeholder="Email" value="<?php echo $email ?>"/>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 no-pad-right">
                            <div class="form-group">
                                <input type="password" name="password" placeholder="Password"/>
                            </div>
                        </div>
                        <div class="col-lg-offset-1 col-lg-3 col-md-3 col-sm-3 col-xs-12 no-padding pull-left">
                            <div class="form-group contactus-btn">
                                <input type="submit" name="submit" class="cntct-btn" value="Login">
                            </div>
                        </div>
                    </form>
                </div>
                <!-- Contact Form Section Ends -->
            </div>

        </div>
    </div>
</div>
